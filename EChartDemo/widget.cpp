﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    WebView(new QWebEngineView),
    WebChannel(new QWebChannel),
    ChObj(new JWebChannel),
    vLayoutMain(new QVBoxLayout),
    hLayoutAddr(new QHBoxLayout)
{
    ui->setupUi(this);
    hLayoutAddr->addWidget(ui->pushButton);
    hLayoutAddr->addWidget(ui->pushButton_2);
    hLayoutAddr->addWidget(ui->pushButton_3);
    hLayoutAddr->addWidget(ui->pushButton_4);
    hLayoutAddr->addWidget(ui->pushButton_5);
    hLayoutAddr->addWidget(ui->pushButton_6);
    hLayoutAddr->addWidget(ui->pushButton_7);
    hLayoutAddr->addWidget(ui->pushButton_8);
    hLayoutAddr->addWidget(ui->pushButton_9);
    hLayoutAddr->addWidget(ui->pushButton_10);
    hLayoutAddr->addWidget(ui->pushButton_11);
    hLayoutAddr->addWidget(ui->pushButton_12);
    hLayoutAddr->addWidget(ui->pushButton_13);
    vLayoutMain->addWidget(WebView);
    vLayoutMain->addLayout(hLayoutAddr);
    setLayout(vLayoutMain);
    WebChannel->registerObject("context", ChObj);
    WebView->page()->setWebChannel(WebChannel);
    connect(ChObj, &JWebChannel::RecvMsg, this, &Widget::RecvJSMeg);
    WebView->load(QUrl("qrc:/file/index.html"));
}

Widget::~Widget()
{
    WebChannel->deregisterObject(ChObj);
    ChObj->deleteLater();
    WebChannel->deleteLater();
    WebView->deleteLater();
    hLayoutAddr->deleteLater();
    vLayoutMain->deleteLater();
    delete ui;
}

// 折线图
void Widget::on_pushButton_clicked()
{
    // 给js传数据
//    ChObj->setProperty("content", "Hello js");
    ChObj->SendMsg(WebView->page(), "hello js");
}

// 折线面积图
void Widget::on_pushButton_2_clicked()
{

}

// 双数值折线
void Widget::on_pushButton_3_clicked()
{

}

// 柱状图
void Widget::on_pushButton_4_clicked()
{
}

// 表中条形图
void Widget::on_pushButton_5_clicked()
{

}

// 彩虹柱状图
void Widget::on_pushButton_6_clicked()
{

}

// 散点图
void Widget::on_pushButton_7_clicked()
{

}

// E线图
void Widget::on_pushButton_8_clicked()
{

}

// 饼图
void Widget::on_pushButton_9_clicked()
{

}

// 雷达图
void Widget::on_pushButton_10_clicked()
{

}

// 和弦图
void Widget::on_pushButton_11_clicked()
{

}

// 仪表盘
void Widget::on_pushButton_12_clicked()
{

}

// 动态数据
void Widget::on_pushButton_13_clicked()
{

}

void Widget::RecvJSMeg(const QString &msg)
{
    qDebug() << "Widget-->" + msg;
}
