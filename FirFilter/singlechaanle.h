﻿#ifndef SINGLECHAANLE_H
#define SINGLECHAANLE_H

#include <QWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QEvent>
#include <QRubberBand>
#include "qcustomplot.h"


class ChWaveInfo{
public:
    int ChNumber;       // 通道号
    float Frequency;    // 采集频率
    float SampTime;     // 采集时长，单位：s
    double Value;        // 值
    ChWaveInfo(int ChNum = 0, float Freq = 0, float Time = 0, double Val = 0) :
        ChNumber(ChNum),
        Frequency(Freq),
        SampTime(Time),
        Value(Val) {}
};

namespace Ui {
class SingleChaanle;
}

class SingleChaanle : public QWidget
{
    Q_OBJECT

public:
    explicit SingleChaanle(QWidget *parent = nullptr);
    ~SingleChaanle();
    QString GetWaveInfo(const ChWaveInfo& Info);
    void SetxAxisRange(double lower, double upper);
    void SetyAxisRange(double lower, double upper);
    void SetData(const QVector<double> &xValue, const QVector<double> &yValue);
private slots:
    void on_Plot_customContextMenuRequested(const QPoint &pos);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent* e);
    // 选中曲线
    void selectionChanged();

protected:
    void Init();
private:
    Ui::SingleChaanle *ui;
protected:
    QRubberBand *RubBand;
    QPoint StartPoint;
    ChWaveInfo WaveInfo;
    double Xup;
    double Xdown;
    double Yup;
    double Ydown;
    double xPos;
    QVector<double> xPosValue;
    QVector<double> yPosValue;
};

#endif // SINGLECHAANLE_H
