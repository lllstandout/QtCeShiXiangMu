﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QThread>
#include "thread1.h"
#include "parsethread.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

signals:
    void StartThread();
    void StartThread2();
    void StopThread();

private slots:
    // 开始线程按钮
    void on_pushButton_clicked();

    // 停止线程按钮
    void on_pushButton_2_clicked();

private:
    Ui::Widget *ui;

    Thread1 *Td1;
    QThread Thread;
};

#endif // WIDGET_H
