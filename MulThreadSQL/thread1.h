﻿#ifndef THREAD1_H
#define THREAD1_H

#include <QObject>
#include "jmysql.h"

class Thread1 : public QObject
{
    Q_OBJECT
public:
    explicit Thread1(QObject *parent = nullptr);

signals:
public slots:
    void DoWork();
};

#endif // THREAD1_H
