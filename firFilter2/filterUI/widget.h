#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "chartwave.h"
#include "../Algorithm/algorithm.h"

namespace Ui {
class Widget;
}

enum DataFileType
{
    FT_TXT,     // 单个文本数据文件，使用逗号 "," 或者空格 " " 或者 TAB "  " 进行分割
    FT_WDAT     // 旋极信息数据采集软件数据格式
};

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_comboBox_2_currentIndexChanged(int index);

    void on_comboBox_3_currentIndexChanged(int index);

    void on_spinBox_valueChanged(int arg1);

    void on_spinBox_2_valueChanged(int arg1);

    void on_spinBox_3_valueChanged(int arg1);

    void on_spinBox_4_valueChanged(int arg1);

    void on_comboBox_currentIndexChanged(int index);

private:
    void initChartWave();
    QVector<double> readDataFile();

private:
    Ui::Widget *ui;

    QString m_fileName;


    // 滤波参数
    FilterType          m_filterType;
    FilterWindowType    m_winType;
    int                 m_winLength;
    double              m_freq;
    double              m_freqLowLimit;
    double              m_freqUpLimit;
    Filter              m_filter;

    // 原始数据
    QVector<double>  m_keys;
    QVector<double>  m_values;

    // 冲击响应数据
    QVector<double>  m_impKeys;
    QVector<double>  m_impValues;

    // 滤波后数据
    QVector<double>  m_fValues;
};

#endif // WIDGET_H
